# -*- coding: utf-8 -*-

# Author : Marion Estoup
# E-mail : marion_110@hotmail.fr
# November 2021

### How to scrape data on the internet 

# Define variables and store the link where you want to scrap data

# Define NewsFeed_AFP with the link where we want to scrape data
NewsFeed_AFP = "https://news.google.com/rss/search?q=source:AFP&um=1&ie=UTF-8&num=100&hl=fr&gl=FR&ceid=FR:fr"

# Define NewsFeed_Reuters with the link where we want to scrape data
NewsFeed_Reuters = "https://news.google.com/rss/search?q=source:Reuters&um=1&ie=UTF-8&num=100&hl=fr&gl=FR&ceid=FR:fr"

# Define NewsFeed_Associated_Press with the link where we want to scrape data
NewsFeed_Associated_Press = "https://news.google.com/rss/search?q=source:AP&um=1&ie=UTF-8&num=100&hl=en-US&gl=US&ceid=US:en"

# Define NewsFeed_Bloomberg with the link where we want to scrape data
NewsFeed_Bloomberg = "https://news.google.com/rss/search?q=source:bloomberg&um=1&ie=UTF-8&num=100&hl=en-US&gl=US&ceid=US:en"

# Define NewsFeed_United_Press_International with the link where we want to scrape data
NewsFeed_United_Press_International = "https://news.google.com/rss/search?q=source:UPI&um=1&ie=UTF-8&num=100&hl=en-US&gl=US&ceid=US:en"



# We install feedparser to be able to scrape data
!pip install feedparser

# We import feedparser and BeautifulSoup to scrape data
import feedparser
from bs4 import BeautifulSoup


# We define a function that will allow us to scrape data 
# Function called NewsFeed_extract 
def NewsFeed_extract(newsfeed):
    NewsFeeds = feedparser.parse(newsfeed) # Use parse from feedparser library to create NewsFeeds
    tab = [NewsFeed.summary for NewsFeed in NewsFeeds.entries] # Define tab with summary of Newsfeeds in entries
    tab_ret = [] # Define empty list in tab_ret that we'll fill with the results
    for link in tab:
        soup = BeautifulSoup(link, 'html.parser') # Define soup to have the link where to find data
        a_tag = soup.find_all('a')[0] # Define a_tag to find data on the link
        if(a_tag.text!=None): # If we have content
            tab_ret.append(a_tag.text) # We add the content to tab_ret to store the result
            
    return tab_ret


## We use the function to scrape data on each link

# Use of the function to scrape data on the link stored in NewsFeed_AFP
# We store the result in news1
news1 = NewsFeed_extract(NewsFeed_AFP)

# Use of the function to scrape data on the link stored in NewsFeed_AReuters
# We store the result in news2
news2 = NewsFeed_extract(NewsFeed_Reuters)

# Use of the function to scrape data on the link stored in NewsFeed_Associated_Press
# We store the result in news3
news3 = NewsFeed_extract(NewsFeed_Associated_Press)

# Use of the function to scrape data on the link stored in NewsFeed_Bloomberg
# We store the result in news4
news4 = NewsFeed_extract(NewsFeed_Bloomberg)

# Use of the function to scrape data on the link stored in NewsFeed_United_Press_International
# We store the result in news5
news5 = NewsFeed_extract(NewsFeed_United_Press_International)

# We can concatenate all results (news1,2,3,4,5) in one variable called news
news = news1 + news2 + news3 + news4 + news5

# And we can put all the sentences in one column and separate them with a comma
news = ','.join(news)
